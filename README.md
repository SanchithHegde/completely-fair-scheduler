# Completely Fair Scheduler (CFS)

The process scheduler used in Linux kernel (since version 2.6.23), simulated 
using Python. The only change is that it uses SortedKeyList instead of a 
red-black tree, but has the same time complexity of operations.

## Installing Dependencies

Run `pip3 install -r requirements.txt` to install the dependencies, with the 
`--user` flag if required.

## Running
```python3 cfs.py```

## Sample Output
```
Enter number of tasks: 5
Enter ID, arrival time, burst time, nice value of processes:
(Times should be in milliseconds)
1 8000 13000 5
2 4730 11500 1
3 3000 12030 4
4 2500 14940 3
5 5000 10950 2

**************** CFS SCHEDULING ****************

╒══════╤════════════════╤══════════════╤════════╤════════════════╤═══════════════════╕
│   ID │   Arrival Time │   Burst Time │   Nice │   Waiting Time │   Turnaround Time │
╞══════╪════════════════╪══════════════╪════════╪════════════════╪═══════════════════╡
│    1 │          8.000 │       13.000 │      5 │         43.920 │            56.920 │
├──────┼────────────────┼──────────────┼────────┼────────────────┼───────────────────┤
│    2 │          4.730 │       11.500 │      1 │         14.350 │            25.850 │
├──────┼────────────────┼──────────────┼────────┼────────────────┼───────────────────┤
│    3 │          3.000 │       12.030 │      4 │         45.586 │            57.616 │
├──────┼────────────────┼──────────────┼────────┼────────────────┼───────────────────┤
│    4 │          2.500 │       14.940 │      3 │         41.156 │            56.096 │
├──────┼────────────────┼──────────────┼────────┼────────────────┼───────────────────┤
│    5 │          5.000 │       10.950 │      2 │         28.160 │            39.110 │
╘══════╧════════════════╧══════════════╧════════╧════════════════╧═══════════════════╛

Average waiting time:  34.634 seconds
Average turnaround time:  47.118 seconds

**************** FCFS SCHEDULING ****************

Average waiting time:  23.814 seconds
Average turnaround time:  36.298 seconds

**************** SJF SCHEDULING ****************

Average waiting time:  21.728 seconds
Average turnaround time:  34.212 seconds

**************** PRIORITY SCHEDULING ****************

Average waiting time:  22.966 seconds
Average turnaround time:  35.450 seconds

**************** ROUND ROBIN SCHEDULING ****************

Average waiting time:  44.660 seconds
Average turnaround time:  57.144 seconds
```

Although FCFS, SJF and Priority scheduling seem to work better for the above
list of tasks, CFS performs better in a real-life situation where new tasks are
constantly being added to the ready queue.